package ru.altair.test.controller

import org.eclipse.persistence.dynamic.DynamicEntity
import org.eclipse.persistence.jaxb.dynamic.DynamicJAXBContext
import org.eclipse.persistence.jaxb.dynamic.DynamicJAXBContextFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import ru.altair.test.CustValidationEventHandler

import javax.xml.bind.JAXBElement
import javax.xml.bind.JAXBException
import javax.xml.bind.JAXBIntrospector
import javax.xml.bind.Unmarshaller

@Controller
class Test {

    DynamicJAXBContext jaxbContext;
    private final defaultIndent = "    ";

    @RequestMapping(value = "/xml", method = RequestMethod.POST)
    @ResponseBody
    public String xml(@RequestParam(value = "xml") String xml,
                      @RequestParam(value = "xsd") String xsd) {
        String pathXsd = saveXSD(xsd);
        String result = process(xml, pathXsd);
        new File(pathXsd).delete();
        return result;
    }

    private String saveXSD(String xsd) {
        def temp = File.createTempFile("scheme", ".xsd")
        temp.write(xsd)
        temp.absolutePath
    }

    private String process(String xml, String xsdPath) {
        String result;
        FileInputStream xsdInputStream = null;
        try {
            xsdInputStream = new FileInputStream(xsdPath);
            jaxbContext = DynamicJAXBContextFactory.createContextFromXSD(xsdInputStream, null, null, null);

            ByteArrayInputStream xmlInputStream = new ByteArrayInputStream(xml.getBytes("UTF-8"));
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setEventHandler(new CustValidationEventHandler());
            Object element = unmarshaller.unmarshal(new ByteArrayInputStream(xml.getBytes("UTF-8")));
            DynamicEntity entity;
            if (element instanceof JAXBElement) {
                entity = JAXBIntrospector.getValue(element);
            } else {
                entity = (DynamicEntity) element;
            }
            return printProperties(entity, "", "");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    private String printProperties(Object element, String nameElement, String indent) {
        String result = "";
        if (element instanceof DynamicEntity) {
            String type = element.getClass().getSimpleName();
            if(nameElement!=null && !nameElement.isEmpty())
                result += "\n"+indent + nameElement;
            for (String name : jaxbContext.getDynamicType(type).getPropertiesNames()) {
                result += printProperties(((DynamicEntity) element).get(name), name, indent + defaultIndent);
            }
        } else if (element instanceof List) {
//            result += "\n" + indent + "List of " + nameElement;
            int i = 1;
            for (Object obj : (List) element) {
                result += "\n" + indent + nameElement+" " + i;
                result += printProperties(obj, "", defaultIndent);
                i++;
            }
//            result += "\n" + indent + "-------------";
        } else {
            result += "\n" + indent + nameElement + " - " + element;
        }
        return result;
    }

}
